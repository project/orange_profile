# Orange Profile

An installation profile for Drupal that includes common modules for projects.

## Contents

* [Introduction](#introduction).
* [Requirements](#requirements).
* [Installation](#installation).
* [Configuration](#configuration).
* [Maintainers](#maintainers).

## Introduction

This profile is part of the Orange Suite developed by [Acro Media Inc.](https://www.acromedia.com/).

This profile installs a bunch of great modules frequently used by modern Drupal sites.
It also contains common configurations for them all.

* For a full description of this profile, visit the [project page](https://www.drupal.org/project/orange_profile).
* To submit bug reports, give feature suggestions, or track changes visit the [issue queue](https://www.drupal.org/project/issues/orange_profile).

## Requirements

Currently, this profile supports Drupal 9.

This profile has no special requirements outside of Composer requirements.

## Installation

Install as you would install any other [contributed Drupal profile](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules) using [Composer](https://getcomposer.org) :

1. Run `composer require drupal/orange_profile` (assuming you have set up the Drupal packages repository).

2. Select this profile when prompted in the Drupal installation process.

## Configuration

There is no further configuration needed for this profile to work correctly.
You will only need to change the configuration to suite your needs.

## Maintainers

Current Maintainers :

* Derek Cresswell - [DerekCresswell](https://www.drupal.org/u/derekcresswell)
* Josh Miller - [joshmiller](https://www.drupal.org/u/joshmiller)

This project sponsored by :

* [Acro Media Inc.](https://www.acromedia.com/)
